## gradle multimodule bootstrapper

1. gradle-java-multimodule - just for java (and spring) based gradle
   subprojects

2. gradle-webjava-multimodule - for java with spring and web/frontend
   subprojects



##### some links to refer -
* http://stackoverflow.com/questions/13868177/apply-configuration-to-specific-projects-in-gradle-build-script
* http://www.makble.com/gradle-configure-selected-subprojects
* http://www.makble.com/configure-gradle-buildscript
